var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    merge = require('merge-stream'),
    browserSync = require('browser-sync'),
    notify = require('gulp-notify');

var path = {
    src: {
        html: 'src/**/*.html',
        css: 'src/css/*.css',
        sass: 'src/sass/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*',
        js: 'src/js/*.js',
    },
    build: {
        html: 'build/',
        css: 'build/css/',
        sass: 'build/css/',
        img: 'build/img/',
        fonts: 'build/fonts/',
        js: 'build/js/',
    },
    watching: {
        html: 'watching/',
        css: 'watching/css/',
        sass: 'watching/css/',
        img: 'watching/img/',
        fonts: 'watching/fonts/',
        js: 'watching/js/',
    }
}


gulp.task('build', function (done) {

    console.log('Началась сборка проекта...');

    gulp.src(path.src.html)
        .pipe(gulp.dest(path.build.html));
    console.log("HTML файлы перенесены в папку build");

    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
    console.log("Шрифты перенесены в папку build/fonts/");

    gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img));
    console.log("Картинки перенесены в папку build/img/");

    gulp.src(path.src.js)
        .pipe(concat('script.js'))
        .pipe(gulp.dest(path.build.js));
    console.log("JS скрипты объединены в один файл script.js и перенесены в папку build/js/");


    var streamCss = gulp.src(path.src.css)
        .pipe(concat('style.css'))
    console.log("CSS стили объединены в один файл style.css и перенесены в папку build/css/");

    var streamSass = gulp.src(path.src.sass)
        .pipe(concat('main.scss'))
        .pipe(sass())
    console.log("SCSS файлы объединены в файл main.scss, преобразованы в main.css и перенесены в папку build/css/");

    merge(streamCss, streamSass)
        .pipe(concat('style.css'))
        .pipe(gulp.dest(path.build.sass));
    console.log("Объединены потоки CSS и SASS в один файл style.css");


    console.log('Cборка проекта закончена.');

    done();
});



/* для режима разработки - отладки */

gulp.task('whtml', function () {
    return gulp.src(path.src.html)
        .pipe(gulp.dest(path.watching.html))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('wjs', function () {
    return gulp.src(path.src.js)
        .pipe(gulp.dest(path.watching.js))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('wimg', function () {
    return gulp.src(path.src.img)
        .pipe(gulp.dest(path.watching.img))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('wfonts', function () {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.watching.fonts))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('wcss', function () {
    return gulp.src(path.src.css)
        .pipe(gulp.dest(path.watching.css))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('wsass', function () {
    return gulp.src(path.src.sass)
        .pipe(sass().on('error', notify.onError({
           message  : "<%= error.message %>",
           title    : "Sass Error!"
        })))
        .pipe(gulp.dest(path.watching.sass))
        .pipe( notify( 'SASS - хорошая работа!' ) )
        .pipe(browserSync.reload({
            stream: true
        }));
});


gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: 'watching'
        },
        notify: false
    });
});

gulp.task('watch', function () {
    gulp.watch(path.src.html, gulp.parallel('whtml'));
    gulp.watch(path.src.css, gulp.parallel('wcss'));
    gulp.watch(path.src.sass, gulp.parallel('wsass'));
    gulp.watch(path.src.js, gulp.parallel('wjs'));
    gulp.watch(path.src.img, gulp.parallel('wimg'));
    gulp.watch(path.src.fonts, gulp.parallel('wfonts'));
});

gulp.task('default', gulp.parallel('whtml', 'wjs', 'wimg', 'wfonts', 'wsass', 'wcss', 'browser-sync', 'watch'));
