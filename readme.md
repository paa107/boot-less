![Preview](preview02.jpg "Preview")

![Preview](preview03.jpg "Preview")

# О чём?

Изучение bootstrap на практике.

## Запуск

### Установка зависимостей

>`npm install`

### Сборка проекта

>`gulp build`

### Отладка проекта (для разработки)

>`gulp`

## Если что-то "пошло не так"

### Установка Gulp

Начиная с Gulp версии 4 и выше в **обязательное** требование вошло глобально установить пакет gulp-cli.

>`npm i gulp-cli -g`

Убедитесь, Вы запускаете проект используя Node.JS версии не ниже 12x. В противном случае проект не запустится. Проверить версию можно коммандой:

> `node -v`

### Доустановить gulp-notify (проверка на ошибка Sass/Scss)

> `npm i gulp-notify -D`


> `npm install jquery`

> `npm i @popperjs/core`

> `npm install bootstrap`
